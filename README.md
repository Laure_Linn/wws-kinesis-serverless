### Description ###

This repository contains the Lambda functions, serverless.yml and Postman collection to install and test the foundation Kinesis architecture and assets.
Postman is used to "post" a payload and initiate the Kinesis producer-consumer process.

### Version ###
1.0.0

### Pre-requisites
Make sure you have Node.js installed on your machine.
```
# To check that nodejs is installed
node -- version
```
Make sure you have npm installed on your machine.
```
# To check that npm is installed
npm -- version
```
Make sure you have the Serverless Framework installed on your machine.
```
# To check that serverless is installed
serverless --version
```
To install the Serverless Framework on your machine:
```
# To install serverless globally
npm install serverless -g
```
Make sure you have one of the aws-sdk's installed on your machine.   
```
# To check that an aws-sdk is installed
aws -- version
```
To install aws-sdk for Node.js on your machine:
```
# To install aws-sdk for Node.js
npm install aws-sdk
```
If and aws-sdk is installed and you get an "command not found" error you need to make sure the path variable includes the directory where the aws-ask is installed.

Make sure you have Postman on your machine.
```
# To download and install Postman
https://www.getpostman.com/
```
Make sure you have admin or developer access to an active Amazon AWS account.
```
Go to the Amazon Web Services home page to login or create an account
```
Make sure you have AWS credentials configured on your machine.  For Mac/Linux the file is located at ~/.aws/credentials.  For Windows the file is located at C:\Users\USERNAME\.aws\credentials for Windows
```
# To setup AWS credentials using CLI
aws configure
```
Configure these AWS attributes:
```
# AWS Configuration attributes
AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Default region name [None]: us-east-1
Default output format [None]: json
```
### Installation
```
npm install
```
### Deploy to AWS
Create the Kinesis stream.
NOTE: This stream must be located in the same region as the Lambda functions.
```
# To create the Kinesis stream
aws kinesis create-stream --stream-name colors --shard-count 1
```
The output should look similar to this:
```
{
    "StreamDescription": {
        "StreamStatus": "CREATING",
        "StreamName": "colors",
        "StreamARN": "arn:aws:kinesis:us-east-1:account-id:stream/colors",
        "Shards": []
    }
}
```
Verify the stream was created.
```
# To verify the stream was created
aws kinesis list-streams
```
The output should look similar to this:
```
{
    "StreamNames": [
        "colors"
    ]
}
```
Log into Serverless Framework (requires a GitHub account).
```
# To login to Serverless.com 
serverless login
```
Deploy the serverless.yml package.
```
# To deploy the serverless yml to Amazon AWS
serverless deploy
```
The output should look similar to this:
```
Serverless: Stack update finished...
Service Information
service: foundation-kinesis
stage: dev
region: us-east-1
stack: foundation-kinesis-dev
api keys:
  None
endpoints:
  POST - https://vsco30zkac.execute-api.us-east-1.amazonaws.com/dev/colors
functions:
  consumer-colors: foundation-kinesis-dev-consumer-colors
  producer-colors: foundation-kinesis-dev-producer-colors
Serverless: Publish service to Serverless Platform...
Service successfully published! Your service details are available at:
```
Copy the POST endpoint that was created to Postman and save the collection.

### Test Deploy

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact