'use strict';

const AWS = require('aws-sdk');
const uuid = require('uuid');

const kinesis = new AWS.Kinesis({
  'region': 'us-east-1'
});

const stream = "colors";

module.exports.handler = (event, context, callback) => {

    const requestBody = JSON.parse(event.body);
    const color = requestBody.color;
    const date = requestBody.date;
    
    if (typeof color !== 'string' ) {
        console.error('Validation Failed');
        callback(null, {
            statusCode: 400,
            headers: { 'Content-Type': 'text/plain' },
            body: 'Couldn\'t create the color.',
        });
        return;
      }

    function SimpleEvent() {
        this.timestamp = new Date().toISOString();
        this.color = color;
    }

    const instance = new SimpleEvent();
    const payload = JSON.stringify(instance);

    const params = {
        Data: payload,
        PartitionKey: uuid.v1(),
        StreamName: stream, 
    };

    kinesis.putRecord(params, function(err, data) {
        if(err) {
            console.log(err, err.stack);
        }
        else {
            console.log(data, params);
        }
    });

    // create a response
    const response = {
        statusCode: 200,
        body: JSON.stringify(params),
      };
      callback(null, response);
} ;

