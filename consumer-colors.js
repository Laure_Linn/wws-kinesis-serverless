'use strict';

const AWS = require('aws-sdk');
const uuid = require('uuid');

const kinesis = new AWS.Kinesis({
    'region': 'us-east-1'
  });

const stream = "foundation-stream-2";

module.exports.handler = function(event, context) {  
    const record = event.Records[0];
    if (record.kinesis) {
        exports.kinesisHandler(event.Records, context);
    } 
};

exports.kinesisHandler = function(records, context) {  
  const data = records
    .map(function(record) {

        let payload = new Buffer(record.kinesis.data, 'base64').toString('utf8');

        let params = {
            Data: payload,
            PartitionKey: uuid.v1(),
            StreamName: stream 
        };
    
        kinesis.putRecord(params, function(err, data) {
            if(err) {
                console.log(err, err.stack);
            }
            else {
                console.log(data, params);
            }
        });
    })
};